/**
 * EnableJS v2.0
 *
 * A jQuery extension which enables live HTML based on data-enable configurations.
 *
 * Add data-enable="[element-id]" any element to toggle any other element.
 * Add data-enable with one of following values:
 * 	tab:		tabs, requires an ancestor element with data-enable="tabgroup"
 * 	charcount:	character count. requires an element with id "rem_chars".
 * 	
 * Use data-enable-cfg to configure behaviour:
 * 	invert: 1|0 	change on/off toggle to off/on
 * 	css: 	string	use another css class instead of the defaults "hidden" and "disabled"
 */
;(function($) {
	var EXT_NAME = 'enable';
	
	$.fn.enable = function(opts) {
		opts = opts||{};
		
		var getElementConfig = function(el) {
			var cfg = {}, arr = ($(el).data(EXT_NAME+'-cfg') && $(el).data(EXT_NAME+'-cfg').length ? $(el).data(EXT_NAME+'-cfg') : '').split('&');
			for(var i=0;i<arr.length;i++) {
				var p = arr[i].split('=');
				cfg[p[0]] = p[1];
			}
			return cfg;
		};
		
		var handle = function(el, e) {
			if(el.is('.fa-enable-bug-fix')) {
				el = el.closest('a');
			}
			
			var enable = (el.data(EXT_NAME)+'').split(',');
			$.each(enable, function(idx, action) {
				var parts = action.split(':'), id = parts[0], act = parts[1];
				
				method = enable;
				if(act && e) {
					method = act;
				} else {
					if(el.is('option') || el.is('select')) {
						method = 'select';
					} else if(el.is(':radio')) {
						method = 'radio';
					}
				}
				
				if(typeof($.fn.enable[method]) == 'function') {
					return $.fn.enable[method](el, id, getElementConfig(el), e);
				} else {
					return $.fn.enable.toggle(el, id, getElementConfig(el), e);
				}
			});
		};
		
		this.on('keyup', ':input', function(e){
			return handle($(e.target), e);
		}).on('change', ':checkbox, :radio, select:has(option[data-'+EXT_NAME+'])', function(e){
			return handle($(e.target), e);
		}).on('click', 'a[href]', function(e){
			return handle($(e.target), e);
		}).on('mouseup', function(e){
			if($(e.target).filter(':not(a[href]):not(:input):not(option):not(select)').length) {
				return handle($(e.target), e);
			}
		}).find('*[data-'+EXT_NAME+']').each(function(){
			return handle($(this));
		});
		
		return this;
	};
	
	var getTargetElement = function(name) {
		var ids = [], cls = [], tmp = name.split(',');
		$.each(tmp, function(idx, v) {
			ids.push('#'+v.split(':')[0]);
			cls.push('.'+v.split(':')[0]);
		});
		
		tgt = $(ids.join(','));
		return tgt.length ? tgt : $(cls.join(','));
	};
	
	$.extend($.fn.enable, {
		'toggle' : function(el, enable, cfg, e) {
			var state;
			
			tgt = getTargetElement(enable);
			if(el.is(':checkbox')) {
				state = cfg.invert === '1' ? (!el.is(':checked')) : (el.is(':checked'));	
				default_cls = tgt.is('button') ? 'disable' : 'hidden';
				
				if(tgt.is('button')) {
					tgt[ state ? 'removeClass' : 'addClass' ](cfg.css && cfg.css.length ? cfg.css : 'disabled');
					el[tgt.hasClass( cfg.css && cfg.css.length ? cfg.css : 'disable' )?'removeClass':'addClass']('active');
				} else {
					if(cfg.css && cfg.css.length) {
						tgt[state ? 'addClass' : 'removeClass'](cfg.css);
					} else {
						tgt[state ? 'removeClass' : 'addClass']('hidden');
					}
				}
			} else if(el.is('a') && tgt.length) {
				if(e) {
					e.preventDefault();
				
					tgt[tgt.hasClass('hidden')?'removeClass':'addClass']('hidden');
					el[tgt.hasClass('hidden')?'removeClass':'addClass']('active');
					
					if(cfg.text && cfg.text.length) {
						var text = cfg.text.split('|');
						if(tgt.is(':visible') && text[1]) {
							el.html(text[1]);
						} else if(!tgt.is(':visible')) {
							el.html(text[0]);
						}
					}
					return true;
				} else {
					el.append('<div class="fa-enable-bug-fix" />');
					el[tgt.hasClass('hidden')?'removeClass':'addClass']('active');
				}
			}
		},
		'tab' : function(el, enable, cfg, e){
			var grp = el.closest('*[data-'+EXT_NAME+'="tabgroup"]'), tabs = grp.find('*[data-'+EXT_NAME+'="tab"]');
					
			var getAttrName = function(el) {
				var name = el.data(EXT_NAME+'-target');
				if(!name.length) {
					name = el.attr('href').replace(/^.*?#/,'');
				}
				return name;
			};
			
			if(e) {
				tabs.removeClass('active').each(function() {
					$('#' + getAttrName($(this))).addClass('hidden');
				});
				el.addClass('active');
				$('#' + getAttrName(el)).removeClass('hidden');
			}
		},
		'select' : function(el) {
			el.find('option[data-'+EXT_NAME+']').each(function(){
				var tgt = getTargetElement($(this).data(EXT_NAME)+'');
				
				tgt.addClass('hidden');
				if($(this).is(':selected')) {
					tgt.removeClass('hidden');
				}
			});
		},
		'charcount' : function(el) {
			tgt = $('#rem_chars');
			if(el.is(':input') && tgt.length) {
				tgt.html(el.attr('maxlength') - el.val().length);
			}
		},
		'radio' : function(el, enable, cfg) {
			
			$('input[name="'+el.attr('name')+'"]').each(function(){
				var state = false;
				
				var tgt = getTargetElement($(this).data(EXT_NAME));
				
				if($(this).is(':checked')) {
					state = true;
				}
			
				if(tgt.length) {
					if(tgt.is('button')) {
						tgt[ state ? 'removeClass' : 'addClass' ](cfg.css && cfg.css.length ? cfg.css : 'disabled');
						el[tgt.hasClass( cfg.css && cfg.css.length ? cfg.css : 'disable' )?'removeClass':'addClass']('active');
					} else {
						if(cfg.css && cfg.css.length) {
							tgt[state ? 'addClass' : 'removeClass'](cfg.css);
						} else {
							tgt[state ? 'removeClass' : 'addClass']('hidden');
						}
					}
				}
			});
		},
		'show' : function(el, enable) {
			tgt = getTargetElement(enable);
			tgt.show();	
		},
		'hide' : function(el, enable) {
			tgt = getTargetElement(enable);
			tgt.hide();	
		},
		'modal' : function(el, enable, cfg, e){
			if(e) {
				e.preventDefault();
				if(window.modal) {
					window.modal.modal('show', el.data('href')||el.attr('href'));
				}
			}
		}
	});
})(jQuery);